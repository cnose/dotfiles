#!/usr/bin/env sh

# set -x
set -euo pipefail

lastRun="$HOME/.config/mutt/.mailchecklastrun"
mailDir="$HOME/.local/share/mail/personal"
if [ "$XDG_SESSION_DESKTOP" = "Hyprland" ]; then
	cmd="kitty --class neomutt neomutt"
else cmd="alacritty --class neomutt -e neomutt"
fi

# verify there is an internet connection
ping -q -c 1 1.1.1.1 > /dev/null || exit

# use mbsync to check for new mail
mbsync -q -q personal || exit

# set variable for new messages checked against an empty file created just for the timestamp
# to ensure new mail notifications occur only once
newcount=$(find "$mailDir"/INBOX/new/ -type f -newer "$lastRun" 2> /dev/null | wc -l)
unread=$(find "$mailDir"/INBOX/new/ -type f 2> /dev/null | wc -l)
echo "$unread"

# if there is new mail, send out a notification
[ "$newcount" -lt "1" ] && exit 0
ACTION=$(paplay "/usr/share/sounds/freedesktop/stereo/complete.oga"&
dunstify -a neomutt -t 8000 -A "open,Open Mail" -i mail "New email" "📬 You have $newcount new mail(s).")

case $ACTION in
	open)
		bash -c "$cmd"&
		;;
	*)
		notmuch new >/dev/null 2>&1 &
		touch "$lastRun" && exit 0
		;;
esac

# sync new emails into notmuch db - for quick searching (honestly I've never used it)
notmuch new >/dev/null 2>&1 &

# create a new dummy file just for the timestamp - see above
touch "$lastRun"

# TODO: make it work in both Qtile and Hyprland
# parallel execution of notification and sound
# move to a systemd service with timer
# change waybar widget to a simple check of the maildir using unread?
