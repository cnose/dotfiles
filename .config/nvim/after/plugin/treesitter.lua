require('nvim-treesitter.configs').setup {
    ensure_installed = { 'bash', 'c', 'css', 'json', 'vim', 'vimdoc', 'lua', 'cpp', 'python', 'yaml', 'markdown', 'nix', 'toml' },

    auto_install = false,

    highlight = { enable = true },

    indent = { enable = true },

	additional_vim_regex_highlighting = false,
}
