-- Set Leader
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Options

vim.o.number = true
vim.o.relativenumber = true

vim.o.signcolumn = 'yes'

vim.o.tabstop = 4
vim.o.softtabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true

vim.o.smartindent = true

vim.o.updatetime = 300

vim.o.termguicolors = true

vim.o.hlsearch = false
vim.o.incsearch = true

vim.o.mouse = 'a'

vim.o.scrolloff = 8
vim.o.ignorecase = true
vim.o.smartcase = true

vim.o.splitbelow = true
vim.o.splitright = true

vim.o.cursorline = true

-- highlight on yank
vim.api.nvim_create_autocmd('TextYankPost', {
  group = vim.api.nvim_create_augroup('highlight_yank', {}),
  desc = 'Highlight selection on yank',
  pattern = '*',
  callback = function()
    vim.highlight.on_yank { higroup = 'IncSearch', timeout = 700 }
  end,
})
