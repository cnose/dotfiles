return {

    {
        "jakobkhansen/journal.nvim",
        config = function()
            require("journal").setup({
                journal = {
                    entries = {
                        week = {
                            template = "# Week %W %B %Y\n\n## %B %d\n\n",
                        },
                    },
                }
            })
        end,
    },
}
