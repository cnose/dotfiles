return {

	{
		"luukvbaal/nnn.nvim",
		config = function()
			local builtin = require("nnn").builtin
			require("nnn").setup({
		mappings = {
		  { "<leader>t", builtin.open_in_tab },
		  { "<leader>s", builtin.open_in_split },
		  { "<leader>v", builtin.open_in_vsplit },
		},
			})
		end,
	}
}
