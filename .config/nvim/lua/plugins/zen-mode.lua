return {

	{
		"folke/zen-mode.nvim",
		opts = {
			window = {
				backdrop = 0.5,
				options = {
					number = false,
					relativenumber = false,
					cursorline = false,
                    list = false,
				},
			},
		},
	},
}
