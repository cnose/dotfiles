-- Select all content in the file
vim.keymap.set("", "<C-a>", "gg<S-v>G", opts)

-- Save, quit, save&quit
vim.keymap.set("", "<leader>w", ":w!<CR>", opts)
vim.keymap.set("", "<leader>x", ":q!<CR>", opts)
vim.keymap.set("", "<leader>q", ":wq<CR>", opts)

-- Splits n tabs nav
vim.keymap.set("n", "<C-h>", ":wincmd h<CR>", opts)
vim.keymap.set("n", "<C-j>", ":wincmd j<CR>", opts)
vim.keymap.set("n", "<C-k>", ":wincmd k<CR>", opts)
vim.keymap.set("n", "<C-l>", ":wincmd l<CR>", opts)
vim.keymap.set("n", "<C-x>", ":wincmd c<CR>", opts)
-- keymap("n", "<C-p>", ":tabp<CR>", opts)
-- keymap("n", "<C-n>", ":tabn<CR>", opts)

-- remove trailing whitespace
-- vim.keymap.set("n", "<leader>tw", [[:%s/\s\+$//e<cr>]])
vim.keymap.set("n", "<leader>tw", [[:lua vim.lsp.buf.format()<cr> <bar> :%s/\s\+$//e<cr>]])

-- nnn mappings
vim.keymap.set("n", "<leader>n", ":NnnPicker<CR>", opts)

--- floaTerm
-- vim.keymap.set("n", "<leader>t", ":FloatermNew<CR>", opts)

-- Zen Mode
vim.keymap.set("n", "<leader>g", ":ZenMode<CR>", opts)

-- nvim tmux config
vim.keymap.set('n', "<M-h>", ":NvimTmuxNavigateLeft<CR>")
vim.keymap.set('n', "<M-j>", ":NvimTmuxNavigateDown<CR>")
vim.keymap.set('n', "<M-k>", ":NvimTmuxNavigateUp<CR>")
vim.keymap.set('n', "<M-l>", ":NvimTmuxNavigateRight<CR>")
vim.keymap.set('n', "<M-\\>", ":NvimTmuxNavigateLastActive<CR>")
vim.keymap.set('n', "<M-Space>", ":NvimTmuxNavigateNext<CR>")

-- stolen from theprimeagen
vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
vim.keymap.set({"n", "v"}, "<leader>y", [["+y]]) -- select # of lines to copy to clipboard
vim.keymap.set("n", "<leader>Y", [["+Y]]) -- copy entire line to clipboard
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
