#!/usr/bin/python
# coding=utf-8

# Nord theme
theme = (
    '#4C566A',
    '#434C5E',
    '#3B4252',
    '#2E3440',
    '#C0C5CE',
    '#EBCB8B',
    '#81A1C1',
    '#ECEFF4',
    '#EFE9F0',
    '#BF616A',
    '#B48EAD',
    '#6790EB',
    '#D8DEE9',
    '#d08770',
    '#a3be8c',
    '#8FBCBB',
    '#88C0D0',
    '#5E81AC',
)

# dm_theme = (
#     "-i -h 26 -nb '#2F3440' -nf '#81A1C1' -sb '#4C566A' -sf '#c0c5ce' -fn MesloLGSNF-12:normal"
# )
