# Qtile config
# by Chris Noseworthy
# (https://gitlab.com/cnose)
#
# modified from the ArcoLinux Qtile config
# (https://arcolinux.info)

import os
import subprocess
from libqtile import qtile
from libqtile import hook
from libqtile.lazy import lazy
from typing import List # noqa: F401
from groups import *
from keys import *
from widgets import *
from layouts import layout_theme, layouts, floating_layout

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

dgroups_app_rules = []

# move to group when spawning into group
@hook.subscribe.client_new
def follow_window_spawn(client):
    for group in groups:
        match = next((m for m in group.matches if m.compare(client)), None)
        if match:
            targetgroup = qtile.groups_map[group.name]
            targetgroup.toscreen(toggle=False)
            break

# move to next occupied group when app in group is closed
@hook.subscribe.client_killed
def fallback(window):
    if window.group.windows != [window]:
        return
    idx = qtile.groups.index(window.group)
    for group in qtile.groups[idx - 1::-1]:
        if group.windows:
            qtile.current_screen.toggle_group(group)
            return
    qtile.current_screen.toggle_group(qtile.groups[0])

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-xcf', '/usr/share/icons/Bibata-Modern-Ice/cursors/left_ptr', '24'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

# TEST CONFIG FROM M-COLS CONF
groupboxes = [
    widget.GroupBox(visible_groups=["1", "2", "3", "4", "5", "6", "7", "8", "9"]),
    widget.GroupBox(visible_groups=["5", "6", "7", "8", "9"]),
]

@hook.subscribe.startup
def _():
    if len(qtile.screens) > 1:
        groupboxes[0].visible_groups = ["1", "2", "3", "4"]
    else:
        groupboxes[0].visible_groups = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

@hook.subscribe.startup
def _():
    # Set initial groups
    if len(qtile.screens) > 1:
        qtile.groups_map["1"].toscreen(0, toggle=False)
        qtile.groups_map["5"].toscreen(1, toggle=False)

@hook.subscribe.screen_change
def _(_):
    # Set groups to screens
    if len(qtile.screens) > 1:
        if qtile.screens[0].group.name not in "1234":
            qtile.groups_map["1"].toscreen(0, toggle=False)
        qtile.groups_map["5"].toscreen(1, toggle=False)
# END TEST CONFIG

floating_types = ["notification", "toolbar", "splash", "dialog"]

follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
auto_fullscreen = True
reconfigure_screens = True
auto_minimize = True

focus_on_window_activation = "smart" # or focus

wmname = "LG3D"
