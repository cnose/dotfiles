#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}


#starting utility applications at boot time
picom --config $HOME/.config/qtile/scripts/picom.conf &
gpg-agent &
run nm-applet &
run volumeicon &
run xfce4-power-manager &
numlockx on &
blueberry-tray &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
# /usr/lib/xfce4/notifyd/xfce4-notifyd &
redshift-gtk -l 43.63:-79.41 &
/usr/bin/kdeconnectd &
kdeconnect-indicator &

#starting user applications at boot time
run alacritty &
libinput-gestures-setup start &
