#!/usr/bin/python
# coding=utf-8

import os
import subprocess
from libqtile import widget, bar, qtile
from libqtile.config import Screen
from libqtile.widget import Spacer
from qtile_extras.widget import UPowerWidget
from colors import theme

home = os.path.expanduser('~')

# def get_weather():
#     return subprocess.check_output(['/usr/bin/curl', 'wttr.in/Riondel?format=2']).decode('utf-8').strip()

def check_mail():
    return subprocess.check_output(['/home/chris/scripts/mailcheck']).decode('utf-8').strip()

# WIDGETS FOR THE BAR

widget_defaults = dict(
    # font="Noto Sans",
    # fontsize = 12,
    # padding = 2,
    background = theme[1],
)

sep_defaults = dict(
    linewidth = 1,
    padding = 10,
    foreground = theme[2],
    background = theme[1],
)

def init_widgets_list():
    widgets_list = [
               widget.GroupBox(
                        **widget_defaults,
                        font="FontAwesome",
                        fontsize = 16,
                        margin_y = 2,
                        margin_x = 0,
                        padding_y = 6,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = theme[2],
                        inactive = theme[0],
                        rounded = False,
                        highlight_method = "line",
                        highlight_color = theme[0],
                        block_highlight_text_color = theme[4],
                        other_screen_border = theme[6],
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.CurrentLayoutIcon(
                        **widget_defaults,
                        custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons/layouts")],
                        foreground = theme[5],
                        padding = 0,
                        scale = 0.5
                        ),
               widget.CurrentLayout(
                        **widget_defaults,
                        font = "Noto Sans Bold",
                        fontsize = 12,
                        foreground = theme[5],
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.WindowName(font="Noto Sans",
                        **widget_defaults,
                        fontsize = 12,
                        foreground = theme[5],
                        ),
                widget.Net(
                         **widget_defaults,
                         font="Noto Sans",
                         fontsize = 12,
                         interface = "wlp3s0",
                         foreground = theme[5],
                         padding = 2,
                         format ='{down:.2f}{down_suffix} ↓↑ {up:.2f}{up_suffix}'
                         ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[6],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.spawn('/home/chris/scripts/cpu')}
                        ),
               widget.CPUGraph(
                        **widget_defaults,
                        border_color = theme[2],
                        fill_color = theme[7],
                        graph_color = theme[8],
                        border_width = 1,
                        line_width = 1,
                        core = "all",
                        type = "box",
                        mouse_callbacks = {'Button1': lambda: qtile.spawn('xfce4-terminal -e btop --geometry=125x45')}
                        ),
               widget.ThermalSensor(
                        **widget_defaults,
                        foreground = theme[2],
                        foreground_alert = theme[6],
                        font = "Noto Sans",
                        fontsize = 12,
                        threshold = 90,
                        update_interval = 5,
                        tag_sensor = "Package id 0"
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[4],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.spawn('/home/chris/scripts/mem')}
                        ),
               widget.Memory(
                        **widget_defaults,
                        font="Noto Sans",
                        format = '{MemUsed: .0f}{mm}/{MemTotal: .0f}{mm}',
                        fontsize = 12,
                        foreground = theme[5],
                        mouse_callbacks = {'Button1': lambda: qtile.spawn('/usr/bin/xfce4-terminal -e btop --geometry=125x45')}
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.Wttr(
                        **widget_defaults,
                        fontsize = 12,
                        foreground = theme[5],
                        font = "Noto Sans",
                        location = {'Toronto': 'YYZ'},
                        units = "m",
                        format = "3",
                        update_interval = 300,
                        mouse_callbacks = {'Button1': lambda: qtile.spawn('xfce4-terminal -e "curl wttr.in/Toronto?m" --geometry=125x45 -H')}
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[7],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks =
                            {'Button1': lambda: qtile.spawn(home + '/scripts/term xfce4-terminal -e neomutt --geometry=125x45'),
                             'Button3': lambda: qtile.spawn(home + '/scripts/mailcheck')}
                        ),
               widget.GenPollText(
                        **widget_defaults,
                        font= "Noto Sans",
                        fontsize = 14,
                        foreground = theme[5],
                        func = check_mail,
                        update_interval = 120
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.TextBox(
                        **widget_defaults,
                        font = "FontAwesome",
                        text = "  ",
                        foreground = theme[3],
                        padding = 0,
                        fontsize = 16,
                        mouse_callbacks = {'Button1': lambda: qtile.spawn(home + '/scripts/term xfce4-terminal -e "calcurse" --geometry=150x50')}
                        ),
               widget.Clock(
                        **widget_defaults,
                        foreground = theme[5],
                        font = "Noto Sans",
                        fontsize = 12,
                        format="%a %h %-d %-I:%M %p",
                        mouse_callbacks = {'Button1': lambda: qtile.spawn(home + '/scripts/term xfce4-terminal -e "khal calendar" -H')}
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               UPowerWidget(
                        **widget_defaults,
                        font = "Noto Sans",
                        fontsize = 12,
                        border_charge_colour = theme[11],
                        border_colour = theme[9],
                        border_critical_colour = theme[6],
                        fill_normal = theme[9],
                        fill_low = theme[10],
                        fill_critical = theme[6],
                        percentage_low = 0.30,
                        percentage_critical = 0.15,
                        text_charging = "({percentage:.0f}%) ttf: {ttf}",
                        text_discharging = "({percentage:.0f}%) tte: {tte}",
                        text_displaytime = 2
                        ),
               widget.Sep(
                        **sep_defaults,
                        ),
               widget.Systray(
                        **widget_defaults,
                        icon_size = 20,
                        padding = 4
                        ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[6:29]
    return widgets_screen2

wall_paper = dict(
    wallpaper = "~/Pictures/nord.png",
    wallpaper_mode = "fill",
)

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=26),**wall_paper),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=26),**wall_paper)]
            # Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=26),**wall_paper)]

# if __name__ in ["widgets", "__main__"]:
screens = init_screens()
# widgets_list = init_widgets_list()
# widgets_screen1 = init_widgets_screen1()
# widgets_screen2 = init_widgets_screen2()
