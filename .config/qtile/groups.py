#!/usr/bin/python
# coding=utf-8
from libqtile.config import Group, Match
from typing import Callable
import re

groups = [Group("1", label="", layout="columns", matches=[Match(wm_class=re.compile(r"^(nnn|nvim|ssh)$"))]),
          Group("2", label="", layout="max", matches=[Match(wm_class=re.compile(r"^(firefox|qutebrowser|brave-browser)$"))]),
          Group("3", label="", layout="max", matches=[Match(wm_class=re.compile(r"^(neomutt)$"))]),
          Group("4", label="", layout="bsp", matches=[Match(wm_class=re.compile(r"^(signal|android-messages-desktop|discord)$"))]),
          Group("5", label="", layout="max", matches=[Match(wm_class=re.compile(r"^(plexmediaplayer|gl|mpv|Plex|vlc)$"))]),
          Group("6", label="", layout="monadtall", matches=[Match(wm_class=re.compile(r"^(youtube\ music|plexamp)$"))]),
          Group("7", label="", layout="max", matches=[Match(wm_class=re.compile(r"^(remote-viewer|Remote-viewer)$"))]),
          Group("8", label="", layout="bsp", matches=[Match(wm_class=re.compile(r"^(thunar)$"))]),
          Group("9", label="", layout="max", matches=[Match(wm_class=re.compile(r"^(org.gnome.meld)$"))])]

def _go_to_group(name: str) -> Callable:
    """
    This creates lazy functions that jump to a given group. When there is more than one
    screen, the first 3 and second 3 groups are kept on the first and second screen.
    E.g. going to the fourth group when the first group (and first screen) is focussed
    will also change the screen to the second screen.
    """

    def _inner(qtile: Qtile) -> None:
        if len(qtile.screens) == 1:
            qtile.groups_map[name].toscreen(toggle=False)
            return

        old = qtile.current_screen.group.name
        if name in "12345":
            qtile.focus_screen(0)
            if old in "12345" or qtile.current_screen.group.name != name:
                qtile.groups_map[name].toscreen(toggle=False)
        else:
            qtile.focus_screen(1)
            if old in "6789" or qtile.current_screen.group.name != name:
                qtile.groups_map[name].toscreen(toggle=False)

    return _inner

